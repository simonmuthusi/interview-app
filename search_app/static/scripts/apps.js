// Code goes here
  (function (document) {
    'use strict';
    document.addEventListener('WebComponentsReady', function() {

        // We have to bind the template with the model
        var t = document.querySelector('#t');

        // chaging the property directly does not reflect in the GUI... :-(
        t.getData = function() {
          t.$.dataAjax.generateRequest();
        }
    });
  })(document);
