from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
import json
from models import SearchItems
from django.http import HttpResponse
from django.db.models import Q


def main_page(request):
    return render(request, "app/index.html", {})


def home_page(request):
    return render(request, "app/index.html", {})


@csrf_exempt
def search_results(request):
    """
    Search for results based on a passed keyword
    """
    keyword = request.POST.get('keyword', '')

    results = {}
    results_list = list()
    for item in SearchItems.objects.filter(Q(search_item__icontains=keyword) | Q(search_description__icontains=keyword)):
        results_list.append(
            {"title": item.search_item, 'description': item.search_description})

    results['results'] = results_list

    return HttpResponse(json.dumps(results))


def not_found_page(request):
    """
    A page for handling 404 errors.
    """
    return HttpResponse("404 Not Found", status=404)


def server_error_page(request):
    """
    A page for handling 500 errors.
    """
    return HttpResponse("500 Server Error", status=500)
