from django.db import models


class SearchItems(models.Model):
    search_item = models.CharField(max_length=1000)
    search_description = models.CharField(max_length=1000)
