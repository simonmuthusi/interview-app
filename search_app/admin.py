from django.contrib import admin
from models import SearchItems

class SearchItemsAdmin(admin.ModelAdmin):
    list_display = ('search_item', 'search_description')
    list_filter = ('search_item','search_description')

admin.site.register(SearchItems, SearchItemsAdmin)
