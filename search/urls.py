from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView, RedirectView
from search_app.views import main_page, home_page

# ]
admin.autodiscover()

urlpatterns = patterns('',

                       url(r'^admin/', include(admin.site.urls)),

                       )

urlpatterns += patterns('search_app.views',

                        url(r'^search/$', 'main_page', name='main_page'),
                        url(r'^search/results/$', 'search_results'),
                        url(r'^$', 'home_page', name='home_page'),

                        )

handler404 = "search.views.not_found_page"
handler500 = "search.views.server_error_page"
